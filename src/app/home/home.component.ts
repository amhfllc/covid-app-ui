import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs';
import { isDevMode } from '@angular/core';
import * as d3 from 'd3';

@Component({
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  Object = Object;
  totals_summary: any;
  data = [];
  data_backup = [];
  totals = [];
  world_obj: any
  selectedDate: any;
  maxDate: any;
  daysToReduceDataBy = 4;
  top10 = {};
  series: any;
  prepareTableHeader = val => val.split('_').map(word => word.charAt(0).toLocaleUpperCase() + word.substring(1)).join(' ');
  sortOrder = {
    "#": "",
    "country": "",
    "confirmed": "desc",
    "new_confirmed": "",
    "active": "",
    "deaths": "",
    "new_deaths": "",
    "recovered": ""
  };
  classes = {
    '': '',
    'desc': 'fas fa-sort-amount-down',
    'asc': 'fas fa-sort-amount-up'
  }

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get(this.buildUrl('/covid/totals')).subscribe((data: any) => {
      if (data && data.length) {
        this.maxDate = data[data.length - 1].date;
        this.totals = data;
        this.onDateChange(this.maxDate);
      }
    });
    this.fetchTopN(10, 'confirmed');
    this.fetchTopN(10, 'active');
    this.fetchTopN(10, 'recovered');
    this.fetchTopN(10, 'deaths');
  }

  searchValueChanged(value: string) {
    this.data = value.trim().length == 0 ?
      this.data_backup : this.data.filter(str => str.country.toLowerCase().startsWith(value.toLowerCase()));
  }

  onDateChange(value: any) {
    if (this.selectedDate != value && this.totals.length) {
      this.selectedDate = value;
      const totals_for_today = this.totals.filter(key => key.date === value)[0];
      this.world_obj = {
        "country": "World",
        "latestData": {
          "deaths": totals_for_today.deaths,
          "confirmed": totals_for_today.confirmed,
          "active": totals_for_today.active,
          "recovered": totals_for_today.recovered,
          "new_deaths": 0,
          "new_confirmed": 0
        },
        "dailyData": null
      };
      forkJoin([this.fetchData('/covid/data', { date: this.selectedDate }), this.fetchData('/covid/data', { date: this.prevDate(this.selectedDate) })])
        .subscribe(responseList => {
          this.data = responseList[0].sort((a, b) => b['latestData'].confirmed - a['latestData'].confirmed);
          this.data_backup = this.data;
          const yesterday_data = this.arrToMap(responseList[1]);
          this.data.forEach((p, i) => {
            const today = p['latestData']
            today['#'] = i + 1;
            const yesterday = yesterday_data[p.country] ? yesterday_data[p.country].latestData : { "deaths": 0, "confirmed": 0 };
            today.new_deaths = today.deaths - yesterday.deaths;
            today.new_confirmed = today.confirmed - yesterday.confirmed;
            this.world_obj.latestData.new_deaths += today.new_deaths;
            this.world_obj.latestData.new_confirmed += today.new_confirmed;
          });
        });
    }
  }

  onColumnSort(value: string) {
    const sortedColState = this.sortOrder[value];
    Object.keys(this.sortOrder).forEach(col => this.sortOrder[col] = "");
    this.sortOrder[value] = sortedColState === "" ? "asc" : sortedColState === "asc" ? "desc" : "asc";
    this.data = this.sortOrder[value] === '' ? this.data_backup : this.data.sort((a, b) => { if (value === 'country') { return a.country.localeCompare(b.country); } else { return a.latestData[value] - b.latestData[value]; } });
    if (this.sortOrder[value] === 'desc') {
      this.data = this.data.reverse();
    }
  }

  getCountrySvg(country: string) {
    const name = country.toLowerCase().split(" ").join("-");
    return "assets/countries-icons/" + name.replace(/[.,\/#!$%\^&\*;:{}=\_'`~()]/g, "") + ".svg";
  }

  private fetchTopN(n: number, property: string) {
    if (n <= 0 || !['recovered', 'deaths', 'active', 'confirmed'].includes(property)) {
      return;
    }
    this.fetchData('/covid/topN', { n: n, property: property }).subscribe(
      (data) => {
        let dailyData = data.map(c => c["dailyData"]);
        let keys = data.map(c => c['country']);
        let seriesData = [];
        for (let i = 0; i < dailyData.length; i++) {
          for (let j = 0; j < dailyData[i].length; j++) {
            seriesData[j] = seriesData[j] ? seriesData[j] : {};
            seriesData[j][keys[i]] = dailyData[i][j][property];
            seriesData[j].date = dailyData[i][j]['date'];
          }
        }
        this.top10[property] = {
          keys: keys,
          data: seriesData.sort((a, b) => a.date.localeCompare(b.date))
        };
      });
  }

  private fetchData(endpoint: string, params: any) {
    const url = this.buildUrl(endpoint);
    return this.http.get<Object[]>(url, { params: params });
  }

  private prevDate(dateStr: string): string {
    let date = new Date(dateStr);
    date.setDate(date.getDate() - 1);
    return date.toISOString().substr(0, 10);
  }

  private buildUrl(uri: string) {
    const prodUrl = 'https://api.covidmeter.co';
    const devUrl = 'http://localhost:8080';
    return (isDevMode() ? devUrl : prodUrl) + uri;
  }

  private arrToMap(data) {
    return data.reduce((map, obj) => {
      map[obj.country] = obj;
      return map;
    }, {});
  }
}
