import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutTabComponent } from './tabs/about-tab.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutTabComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
