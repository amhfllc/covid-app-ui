import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppDatePicker } from './design-components/app-date-picker.component';
import { AppService } from './app.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataVisualizer } from './design-components/data-visualizer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AppDatePicker,
    DataVisualizer
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
