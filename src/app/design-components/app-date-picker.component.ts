import { Component, Output, EventEmitter, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-date-picker',
  templateUrl: './app-date-picker.html'
})
export class AppDatePicker implements OnInit, OnChanges {
  @Input() maxDate: string;
  @Output() modelValue = new EventEmitter();
  model: NgbDateStruct;
  minDate = {year: 2020, month: 1, day: 22};
  calendar: NgbCalendar;

  constructor(calendar: NgbCalendar) {
    this.calendar = calendar;
  }

  ngOnInit(): void {
    this.onModelChange(this.calendar.getToday());
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['maxDate'] && this.maxDate) {
      this.model = this.getNgbDate(this.maxDate);
    }
  }

  onModelChange($event: any) {
    this.model = $event;
    this.modelValue.emit(this.getIsoDateFormat());
  }

  getNgbDate(dateStr?: string) {
    const date = dateStr ? new Date(dateStr) : new Date();
    return {
      year: date.getFullYear(),
      month: date.getMonth()+1,
      day: date.getDate()
    };
  }

  private getIsoDateFormat(): string {
    const value = this.model;
    const day = value.day < 10 ? "0" + value.day : value.day;
    const month = value.month < 10 ? "0" + value.month : value.month;
    return value.year + "-" + month + "-" + day;
  }
}
