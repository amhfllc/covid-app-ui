import { OnInit, ViewChild, ElementRef, Input, Component, OnChanges, SimpleChanges } from '@angular/core';
import * as d3 from 'd3';
import * as swatches from './swatches.js';
import html from '@observablehq/stdlib/src/html.js';

@Component({
  selector: "data-visualizer",
  template: `<div #mainChart class="chart"></div>`
})
export class DataVisualizer implements OnChanges {
  @ViewChild('mainChart')
  private chartContainer: ElementRef;

  @Input()
  dataset: any;

  @Input()
  reductionFactor: number = 7;

  @Input()
  colors: Array<any>;

  @Input()
  keys: Array<any>;

  @Input()
  isCumulative = true;

  @Input()
  title = '';

  @Input()
  series: any;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.chartContainer && this.dataset) {
      this.setupChart();
    }
  }

  private setupChart() {
    const element = this.chartContainer.nativeElement;
    const data = this.reduceDataset(this.dataset, this.reductionFactor);
    const margin = ({ top: 10, right: 10, bottom: 70, left: 40 });
    const height = 400;
    const width = 600;
    const formatValue = x => isNaN(x) ? "N/A" : x.toLocaleString("en");
    const series = this.series ? this.series : d3.stack()
      .keys(this.keys)(data)
      .map(d => (d.forEach(v => v.key = d.key), d));

    const x = d3.scaleBand()
      .domain(data.map(d => d.date))
      .range([margin.left, width - margin.right])
      .padding(0.1);

    const y = d3.scaleLinear()
      .domain([d3.min(series, d => d3.min(d, d => d[1])), d3.max(series, d => d3.max(d, d => d[1]))])
      .rangeRound([height - margin.bottom, margin.top]);

    const xAxis = g => g
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(x).tickSizeOuter(0))
      .call(g => g.selectAll(".domain").remove());

    const yAxis = g => g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y).ticks(null, "s"))
      .call(g => g.selectAll(".domain").remove());

    const color = d3.scaleOrdinal()
      .domain(this.keys)
      .range(this.colors ? this.colors : d3.schemeSpectral[series.length])
      .unknown("#ccc");

    this.legendGenerator(element, color);
    const svg = d3.select(element).append("svg")
      .attr("viewBox", [0, 0, width, height]);

    svg.append("g")
      .selectAll("g")
      .data(series)
      .join("g")
      .attr("fill", d => color(d.key))
      .selectAll("rect")
      .data(d => d)
      .join("rect")
      .attr("x", (d, i) => x(d.data.date))
      .attr("y", d => y(d[1]))
      .attr("height", d => y(d[0]) - y(d[1]))
      .attr("width", x.bandwidth())
      .append("title")
      .text(d => this.keys.map(key => `${this.capitalize(key)}: ${formatValue(d.data[key])}`).join("\n"));

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll("text")
      .style("text-anchor", "start")
      .attr("dy", ".15em")
      .attr("y", "0")
      .attr("x", "-6em")
      .attr("transform", "rotate(-60)");

    svg.append("g")
      .call(yAxis);
  }

  private legendGenerator(element, color) {
    const titleElement = html`
      <div class="chart-title-bar"><span>${this.title}</span></div>
    `;
    const legendElement = swatches.swatches({
      color: color,
      format: (x) => this.capitalize(x),
      swatchSize: 16,
      marginLeft: 50
    });
    element.append(titleElement);
    element.append(legendElement);
  }

  /**
   * Our dataset is cumulative in nature. If we want non-cumulative data,
   * we just set the flag to off.
   * @param dataset 
   * @param days 
   */
  private reduceDataset(dataset: any, days: number) {
    const datasetClone = JSON.parse(JSON.stringify(dataset));
    let arr = [];
    for (let i = 0; i < datasetClone.length; i += days) {
      arr.push(datasetClone[i]);
    }
    if ((datasetClone.length - 1) % days > 0) {
      arr.push(datasetClone[datasetClone.length - 1]);
    }
    if (this.isCumulative) {
      return arr;
    }

    for (let i = arr.length - 1; i > 0; i--) {
      this.keys.forEach(key => {
        arr[i][key] -= arr[i - 1][key];
      });
    }
    return arr;
  }

  private capitalize(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
}